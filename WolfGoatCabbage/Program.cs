﻿using System;

namespace WolfGoatCabbage
{
    class Program
    {
        static void Main(string[] args)
        {
            Wgc test = new Wgc();
            while (true)
            {
                test.Status();
                Console.WriteLine("Выберите действие:\n1 - Посадить на лодку\n2 - Высадить с лодки\n3 - Переплыть на другой берег");
                switch (Console.ReadKey().KeyChar)
                {
                    case '1':
                        {
                            test.FromSideToBoat();

                            if (test.CheckedRules())
                            {
                                Console.Clear();
                                test.Status();
                                test.CheckedRules();
                                return;
                            }
                            break;
                        }
                    case '2':
                        {
                            test.FromBoatToSide();
                            if (test.CheckWon())
                            {
                                Console.Clear();
                                test.Status();
                                test.CheckedRules();
                                return;
                            }
                            break;
                        }
                    case '3':
                        {
                            test.ChangeSide();

                            if (test.CheckedRules())
                            {
                                Console.Clear();
                                test.Status();
                                return;
                            }
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Неверный ввод!");
                            break;
                        }
                }
                Console.Clear();
            }
        }
    }
}
