﻿using System;
using System.Collections.Generic;

namespace WolfGoatCabbage
{
    class Wgc
    {
        List<Content> leftSide = new List<Content>{Content.Goat, Content.Cabbage, Content.Wolf};
        List<Content> rightSide = new List<Content>();
        BoatSide boatSide = BoatSide.Left;
        Content boatContent = Content.Empty;

        public enum BoatSide
        {
            Left,
            Right
        }

        public enum Content
        {
            Empty,
            Wolf,
            Goat,
            Cabbage
        }

        public void FromSideToBoat()
        {
            OnBoat(boatSide == BoatSide.Left ? leftSide : rightSide);
        }

        public void FromBoatToSide()
        {
            OutBoat(boatSide == BoatSide.Left ? leftSide : rightSide);
        }

        private void OnBoat(List<Content> a)
        {
            if (boatContent == Content.Empty)
            {
                Console.WriteLine("\nЧто погрузить в лодку?");
                Console.WriteLine("1 - Волк");
                Console.WriteLine("2 - Коза");
                Console.WriteLine("3 - Капуста");
                char c = Console.ReadKey().KeyChar;
                if (a.Contains((Content) (Convert.ToInt32(c) - 48)))
                {
                    boatContent = (Content) (Convert.ToInt32(c) - 48);
                    a.Remove((Content)(Convert.ToInt32(c) - 48));
                }
                else
                {
                    Console.WriteLine("\nЭтого объекта нет на этом берегу!");
                }
            }
            else
            {
                Console.WriteLine("Лодка не пуста!");
            }
        }

        private void OutBoat(List<Content> a)
        {
            if (boatContent!=Content.Empty)
            {
                a.Add(boatContent);
                boatContent = Content.Empty;
            }
        }

        public void ChangeSide()
        {
            boatSide = boatSide == BoatSide.Left ? BoatSide.Right : BoatSide.Left;
        }

        private bool CheckedSides(List<Content> side)
        {
            if (side.Count == 2)
            {
                if (side.Contains(Content.Wolf) && side.Contains(Content.Goat))
                {
                    Console.WriteLine("\nВы проиграли! Волк съел козу.");
                    return true;
                }

                if (side.Contains(Content.Goat) && side.Contains(Content.Cabbage))
                {
                    Console.WriteLine("\nВы проиграли! Коза съела капусту.");
                    return true;
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        public bool CheckWon()
        {
            bool isWon = false;
            if (leftSide.Count == 0 && rightSide.Count == 3)
            {
                Console.WriteLine("\nПоздравляю! Вы выиграли!");
                isWon = true;
            }

            return isWon;
        }

        public bool CheckedRules()
        {
            bool leftCheck = CheckedSides(leftSide);
            bool rightCheck = CheckedSides(rightSide);
            bool isWon = CheckWon();
            return (isWon == true || leftCheck == true || rightCheck == true);
        }

        public void Status()
        {
            Console.WriteLine("Задача: переправить волка, козу и капусту на другой берег. За один раз перевезти можно только кого-то одного. \nЕсли коза и капуста останутся на одном берегу - коза съест капусту. \nЕсли волк и коза останутся на одном берегу - волк съест козу.\n");
            string leftSideStatus = "На левой стороне находятся:" + ContentSideInfo(leftSide);
            string rightSideStatus = "На правой стороне находятся:" + ContentSideInfo(rightSide);
            Console.WriteLine(leftSideStatus);
            Console.WriteLine(rightSideStatus);
            Console.WriteLine("В лодке:" + ContentBoatInfo(boatContent));
            Console.WriteLine(boatSide == BoatSide.Left ? "Лодка на левом берегу" : "Лодка на правом берегу");
        }

        private string ContentSideInfo(List<Content> side)
        {
            string info = null;
            if (side.Contains(Content.Cabbage))
            {
                info += " капуста";
            }
            if (side.Contains(Content.Goat))
            {
                info += " коза";
            }
            if (side.Contains(Content.Wolf))
            {
                info += " волк";
            }
            return info;
        }

        private string ContentBoatInfo(Content side)
        {
            string info = null;
            if (side == Content.Cabbage)
            {
                info += " капуста";
            }
            if (side == Content.Goat)
            {
                info += " коза";
            }
            if (side == Content.Wolf)
            {
                info += " волк";
            }
            if (side == Content.Empty)
            {
                info += " пусто";
            }
            return info;
        }
    }
}
